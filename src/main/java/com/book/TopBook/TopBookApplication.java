package com.book.TopBook;

import com.book.TopBook.book.domain.Book;
import com.book.TopBook.book.repository.BookRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import javax.annotation.PostConstruct;

@SpringBootApplication
public class TopBookApplication {

	@Autowired
	private BookRepository bookRepository;

	public static void main(String[] args) {
		SpringApplication.run(TopBookApplication.class, args);
	}

	@PostConstruct
	public void initMockData() {
		bookRepository.save(new Book("Some title1", "Some description1"));
		bookRepository.save(new Book("Some title2", "Some description2"));
		bookRepository.save(new Book("Some title3", "Some description3"));
	}
}
