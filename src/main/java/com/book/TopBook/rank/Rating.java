package com.book.TopBook.rank;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

/**
 * Created by demo094 on 2018-02-17.
 */
@Entity
public class Rating {

	@Id
	@GeneratedValue
	private Long id;
	private Integer stars;
	private String description;

	public Rating() {
	}

	public Integer getStars() {
		return stars;
	}

	public void setStars(Integer stars) {
		this.stars = stars;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}
}
