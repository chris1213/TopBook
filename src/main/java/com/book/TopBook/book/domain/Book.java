package com.book.TopBook.book.domain;

import javax.persistence.*;

/**
 * Created by demo094 on 2018-02-15.
 */
@Entity
public class Book {

	@Id
	@GeneratedValue
	private Long id;

	private String title;
	private String description;

	@OneToMany
	private Category category;

	public Book(String title, String description) {
		this.title = title;
		this.description = description;
	}

	public Book() {
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Long getId() {
		return id;
	}

	public Category getCategory() {
		return category;
	}

	public void setCategory(Category category) {
		this.category = category;
	}
}
