package com.book.TopBook.book.domain;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

/**
 * Created by demo094 on 2018-02-17.
 */
@Entity
public class Category {

	@Id
	@GeneratedValue
	private Long id;

	private String name;


	public Category(String name) {
		this.name = name;
	}

	public Category() {
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}
}
